﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

namespace Nudle.Scripts.AI
{
    public class Rabbit : MonoBehaviour
    {
        public float DetectionRadius;
        public float WalkSpeed;
        public float FleeSpeed;

        public Transform[] HidingSpots;
        public NavMeshAgent Agent;

        private float Timer;
        private bool Fleeing;

        internal void Start()
        {
            Timer = 0f;
            Fleeing = false;
        }

        internal void Update()
        {
            if (!Fleeing)
            {
                Agent.speed = WalkSpeed;
                if (Timer <= 0f)
                {
                    Agent.SetDestination(FindRandomTarget());
                    Timer = 5f;
                }
                Timer -= Time.deltaTime;
                if (PlayerIsInRange())
                {
                    Flee();
                }
            }
            else
            {
                if (Agent.remainingDistance < 0.5f)
                {
                    Timer = 0f;
                    Agent.Warp(HidingSpots[Random.Range(0, HidingSpots.Length)].position);
                    Fleeing = false;
                }
            }
        }

        private Vector3 FindRandomTarget()
        {
            var x = transform.position.x + Random.Range(-DetectionRadius, DetectionRadius);
            var z = transform.position.z + Random.Range(-DetectionRadius, DetectionRadius);
            var y = Terrain.activeTerrain.SampleHeight(new Vector3(x, 0, z));
            return new Vector3(x, y, z);
        }

        private bool PlayerIsInRange() => Vector3.Distance(GetPlayerPosition(), transform.position) <= DetectionRadius;

        private void Flee()
        {
            var hidingSpot = FindHidingSpot();
            if (hidingSpot == default)
                hidingSpot = HidingSpots[Random.Range(0, HidingSpots.Length)].position;
            Agent.SetDestination(hidingSpot);
            Agent.speed = FleeSpeed;
            Fleeing = true;
        }

        private Vector3 FindHidingSpot()
            => GetValidHidingSpots().OrderBy(pos => Vector3.Distance(pos, transform.position)).FirstOrDefault();

        private IEnumerable<Vector3> GetValidHidingSpots()
        {
            var idealDirection = (transform.position - GetPlayerPosition()).normalized;
            return HidingSpots.Where(spot => Vector3.Angle(idealDirection, spot.position - transform.position) < 90f)
                                        .Select(t => t.position);
        }

        private Vector3 GetPlayerPosition() => GameObject.FindGameObjectWithTag("Player").transform.position;

        internal void OnDrawGizmosSelected()
        {
            if (!Fleeing)
            {
                Gizmos.color = Color.green;
                Gizmos.DrawWireSphere(transform.position, DetectionRadius);
                Gizmos.color = Color.yellow;
                Gizmos.DrawLine(transform.position, Agent.destination);
                foreach (var spot in GetValidHidingSpots())
                {
                    Gizmos.color = Color.blue;
                    Gizmos.DrawLine(transform.position, spot);
                }
            }
            else
            {
                Gizmos.color = Color.red;
                Gizmos.DrawLine(transform.position, Agent.destination);
            }
        }
    }
}