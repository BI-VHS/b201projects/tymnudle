﻿using UnityEngine;

namespace Nudle.Scripts
{
    public class FloatingText : MonoBehaviour
    {
        public Transform Source;
        public Vector2 Offset;

        private RectTransform Transform;

        internal void Start()
        {
            Transform = GetComponent<RectTransform>();
        }

        internal void Update()
        {
            var screenPos = Camera.main.WorldToScreenPoint(Source.position);
            Transform.anchoredPosition = new Vector2(screenPos.x / Screen.width * 1920, screenPos.y / Screen.height * 1080) + Offset;
        }
    }
}
