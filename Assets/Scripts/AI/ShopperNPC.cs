﻿using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

namespace Nudle.Scripts.AI
{
    public class ShopperNPC : MonoBehaviour
    {
        public Transform[] Stalls;
        public Transform Gate;
        public Transform Home;
        public NavMeshAgent Agent;
        public Animator Animator;

        private static int PeopleShopping = 0;

        [SerializeField]
        private Vector3[] path;

        [SerializeField]
        private int pathIndex;

        [SerializeField]
        private bool stopped;

        [SerializeField]
        private bool leavingTown;

        internal void Start()
        {
            gameObject.transform.position = new Vector3(100f, 0f, 100f);
            StartCoroutine(StopFor(Mathf.Clamp(Random.Range(0f, 8f) - 1f, 0f, 20f)));
        }

        internal void Update()
        {
            if (stopped)
                return;

            if ((path is null || path.Length == 0) && PeopleShopping < 5)
            {
                if (!leavingTown)
                    gameObject.transform.position = Home.position;
                else
                    gameObject.transform.position = Gate.position;
                path = BuildPath();
                pathIndex = 0;
                Agent.enabled = true;
                Agent.isStopped = false;
                Agent.SetDestination(path[pathIndex]);
                if (path.Length > 1)
                    ++PeopleShopping;
            }
            if (!Agent.pathPending && Agent.enabled && Agent.remainingDistance < 0.3f && path != null && path.Length > 0)
            {
                ++pathIndex;
                if (pathIndex == path.Length - 1)
                    PeopleShopping = PeopleShopping == 0 ? 0 : PeopleShopping - 1;
                if (pathIndex >= path.Length)
                {
                    StartCoroutine(StopFor(10f + Random.Range(0f, 7f)));
                    path = null;
                    Agent.ResetPath();
                    Agent.enabled = false;
                    gameObject.transform.position = new Vector3(100f, 0f, 100f);
                }
                else
                {
                    Agent.SetDestination(path[pathIndex]);
                    StartCoroutine(StopFor(3f + Random.Range(0f, 3f)));
                }
            }
        }

        private Vector3[] BuildPath()
        {
            var stallCount = Random.Range(0, Mathf.Min(3, Stalls.Length + 1));
            if (stallCount > 0)
            {
                leavingTown = false;
                return Util.FisherYatesShuffle(Stalls, stallCount).Select(tf => tf.position).Union(new Vector3[] { Home.position }).ToArray();
            }
            if (leavingTown)
            {
                leavingTown = false;
                return new Vector3[] { Home.position };
            }
            leavingTown = true;
            return new Vector3[] { Gate.position };
        }

        public IEnumerator StopFor(float seconds)
        {
            Agent.isStopped = true;
            stopped = true;
            Animator.SetBool("isWalking", false);
            yield return new WaitForSeconds(seconds);
            if (Agent.enabled)
                Agent.isStopped = false;
            stopped = false;
            Animator.SetBool("isWalking", true);
        }
    }
}