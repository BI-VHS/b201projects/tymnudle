﻿using UnityEngine;
using Nudle.Scripts.Dialogue;

namespace Nudle.Scripts
{
    public class SceneFixerTownHall : MonoBehaviour
    {
        public GameObject Mayor;
        public GameObject MayorTrigger;
        public GameObject Scribe;
        public GameObject ScribeTrigger;

        internal void Start()
        {
            if (DialogueUtil.IsFlagSet("Mayor_Dead"))
                KillMayor();
            if (DialogueUtil.IsFlagSet("Scribe_Dead"))
                KillMayor();
        }

        public void KillMayor()
        {
            if (!DialogueUtil.IsFlagSet("Mayor_Dead"))
                return;
            Mayor.GetComponent<Animator>().enabled = false;
            MayorTrigger.SetActive(false);
        }

        public void KillScribe()
        {
            if (!DialogueUtil.IsFlagSet("Scribe_Dead"))
                return;
            Scribe.GetComponent<Animator>().enabled = false;
            ScribeTrigger.SetActive(false);
        }
    }
}
