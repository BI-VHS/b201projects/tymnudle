using UnityEngine;
using UnityEngine.AI;

namespace Nudle.Scripts.AI
{
    public class Patron : MonoBehaviour
    {
        public Transform BeerPosition;
        public Transform SeatPosition;
        public Transform SittingTransform;
        public BeerManager Manager;
        public Animator Animator;
        public GameObject Beer;

        [SerializeField]
        private AIState State = AIState.SEEKING_BEER;

        private float Timer;
        private NavMeshAgent Agent;

        public float DrinkingTime = 25f;
        public float SleepTime = 6f;

        private readonly float LerpTotalTime = 1.2f;
        private float LerpTime;
        private Vector3 OriginalPos;
        private Vector3 OriginalRot;

        internal void Start()
        {
            Agent = GetComponent<NavMeshAgent>();
        }

        internal void Update()
        {
            switch (State)
            {
                case AIState.SITTING_WITHOUT_BEER:
                    SittingWithoutBeer();
                    break;

                case AIState.GETTING_UP:
                    GettingUp();
                    break;

                case AIState.SEEKING_BEER:
                    SeekingBeer();
                    break;

                case AIState.RETURNING_TO_SEAT:
                    ReturningToSeat();
                    break;

                case AIState.SITTING_DOWN:
                    SittingDown();
                    break;

                case AIState.SITTING_WITH_BEER:
                    SittingWithBeer();
                    break;

                case AIState.DRINKING:
                    DrinkingBeer();
                    break;
            }
        }

        private void SittingWithBeer()
        {
            Timer -= Time.deltaTime;
            if (Timer <= 0)
            {
                State = AIState.DRINKING;
                Timer = DrinkingTime;
            }
        }

        private void SittingWithoutBeer()
        {
            Timer -= Time.deltaTime;
            if (Timer <= 0 && Manager.AvailibleBeers() > 0)
            {
                State = AIState.GETTING_UP;
                Animator.SetTrigger("StandUp");
                Animator.ResetTrigger("Sit");
                LerpTime = 0;
                OriginalPos = gameObject.transform.position;
                OriginalRot = gameObject.transform.rotation.eulerAngles;
            }
        }

        private void DrinkingBeer()
        {
            if (Timer > 19f && Timer < 21f)
                Animator.SetTrigger("Drink");
            Timer -= Time.deltaTime;
            Beer.SetActive(Animator.GetCurrentAnimatorStateInfo(0).IsTag("Drinking"));
            if (Timer <= 0)
            {
                State = AIState.SITTING_WITHOUT_BEER;
                Timer = SleepTime;
            }
        }

        private void SeekingBeer()
        {
            if (Agent.remainingDistance <= Agent.stoppingDistance + 0.01f && Manager.AvailibleBeers() > 0)
            {
                Animator.SetTrigger("Grab");
                if (Animator.GetNextAnimatorStateInfo(0).IsName("WalkingBeer"))
                {
                    Animator.ResetTrigger("Grab");
                    State = AIState.RETURNING_TO_SEAT;
                    Agent.destination = SeatPosition.position;
                    Manager.SendMessage("RemoveBeer");
                }
            }
        }

        private void ReturningToSeat()
        {
            if (Agent.remainingDistance <= Agent.stoppingDistance + 0.01f)
            {
                State = AIState.SITTING_DOWN;
                Animator.SetBool("isWalking", false);
                Agent.enabled = false;
                Animator.SetTrigger("Sit");
                Animator.ResetTrigger("StandUp");
                LerpTime = 0;
                OriginalPos = gameObject.transform.position;
                OriginalRot = gameObject.transform.rotation.eulerAngles;
            }
            else
            { 
                Beer.SetActive(true);
            }
        }

        private void SittingDown()
        {
            LerpTime += Time.deltaTime;
            gameObject.transform.position = Vector3.Lerp(OriginalPos, SittingTransform.position, LerpTime / LerpTotalTime);
            gameObject.transform.rotation = Quaternion.Euler(Vector3.Lerp(OriginalRot, SittingTransform.rotation.eulerAngles, LerpTime / LerpTotalTime));
            if (Animator.GetCurrentAnimatorStateInfo(0).IsTag("SittingIdle"))
            {
                State = AIState.SITTING_WITH_BEER;
                Timer = SleepTime;
            }
        }

        private void GettingUp()
        {
            if (Animator.GetCurrentAnimatorStateInfo(0).IsTag("Idle"))
            {
                State = AIState.SEEKING_BEER;
                Agent.enabled = true;
                Agent.destination = BeerPosition.position;
                Animator.SetBool("isWalking", true);
            }
        }

        private enum AIState
        {
            SITTING_WITHOUT_BEER,
            GETTING_UP,
            SEEKING_BEER,
            RETURNING_TO_SEAT,
            SITTING_DOWN,
            SITTING_WITH_BEER,
            DRINKING,
        }
    }
}