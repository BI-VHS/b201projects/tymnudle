﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Nudle.Scripts
{
    public static class Util
    {
        internal static IEnumerable<T> FisherYatesShuffle<T>(IList<T> from, int elements)
        {
            for (var elementIndex = 0; elementIndex < from.Count; ++elementIndex)
            {
                var randomIndex = Random.Range(0, elementIndex);

                var currentValue = from[elementIndex];

                from[elementIndex] = from[randomIndex];

                from[randomIndex] = currentValue;
            }

            return from.Take(elements);
        }

        internal static IEnumerator In<Arg>(float seconds, Action<Arg> action, Arg arg)
        {
            yield return new WaitForSeconds(seconds);
            action.Invoke(arg);
        }

        internal static IEnumerator In(float seconds, Action action)
        {
            yield return new WaitForSeconds(seconds);
            action.Invoke();
        }

        internal static IEnumerator DeleteAfter(GameObject toDelete, float after)
        {
            yield return new WaitForSeconds(after);
            UnityEngine.Object.Destroy(toDelete);
        }
    }
}