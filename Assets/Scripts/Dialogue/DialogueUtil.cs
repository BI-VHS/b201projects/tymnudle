﻿using System;
using System.Collections.Generic;

namespace Nudle.Scripts.Dialogue
{
    public static class DialogueUtil
    {
        private static readonly HashSet<string> Flags = new HashSet<string>(StringComparer.OrdinalIgnoreCase);

        public static bool IsFlagSet(string flag) => Flags.Contains(flag);

        public static void SetFlag(string flag) => Flags.Add(flag);

        public static void UnsetFlag(string flag) => Flags.Remove(flag);

        public static bool AreAllSet(string[] flags)
        {
            bool result = true;
            foreach (var flag in flags)
                result &= IsFlagSet(flag);
            return result;
        }
        public static bool AreAllUnset(string[] flags)
        {
            bool result = true;
            foreach (var flag in flags)
                result &= !IsFlagSet(flag);
            return result;
        }

        public static void ClearFlags() => Flags.Clear();

        internal static IEnumerable<string> GetFlags() => Flags;
    }
}
