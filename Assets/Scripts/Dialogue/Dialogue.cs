﻿using UnityEngine;

namespace Nudle.Scripts.Dialogue
{
    [CreateAssetMenu(fileName = "Dialogue", menuName = "ScriptableObjects/Dialogue", order = 1)]
    public class Dialogue : ScriptableObject
    {
        public string CharacterName;
        public Choice[] Choices;
        public Dialogue NextDialogue;

        [TextArea]
        public string[] Lines;

        public string[] RequiredSetFlags;
        public string[] RequiredUnsetFlags;

        public string[] SetFlags;
        public string[] UnsetFlags;
    }
}