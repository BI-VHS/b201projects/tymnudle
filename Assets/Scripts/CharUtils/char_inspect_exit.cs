using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class char_inspect_exit : MonoBehaviour
{

    public Animator playerAnimator;

    void Start()
    {
        playerAnimator.SetBool("isInspecting", false);
    }
    void Execute()
    {
        playerAnimator.SetBool("isInspecting", false);
    }
}
