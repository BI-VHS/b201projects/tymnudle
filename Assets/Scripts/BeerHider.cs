﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Nudle.Scripts
{
    public class BeerHider : MonoBehaviour
    {
        public Animator Animator;
        public GameObject Beer;

        internal void Update()
        {
            Beer.SetActive(Animator.GetCurrentAnimatorStateInfo(0).IsTag("Drinking"));
        }
    }
}
