﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailerCamera : MonoBehaviour
{
	public float XSpeed = 0;
	public float YSpeed = 0;
	public float ZSpeed = 0;

    void Update()
    {
		Vector3 vector = transform.position;
		Vector3 speed = new Vector3(XSpeed*Time.deltaTime, YSpeed*Time.deltaTime,ZSpeed*Time.deltaTime);
		transform.position = vector + speed;
    }
}
