﻿using UnityEngine;

namespace Nudle.Scripts.Dialogue
{
    [CreateAssetMenu(fileName = "Choice", menuName = "ScriptableObjects/Choice", order = 2)]
    public class Choice : ScriptableObject
    {
        public string Text;
        public string[] RequiredSetFlags;
        public string[] RequiredUnsetFlags;
        public Dialogue NextDialogue;
    }
}