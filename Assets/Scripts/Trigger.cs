﻿using Nudle.Scripts.Dialogue;
using UnityEngine;
using UnityEngine.Events;

namespace Nudle.Scripts
{
    public class Trigger : MonoBehaviour
    {
        public string[] RequiredSetFlags;
        public string[] RequiredUnsetFlags;

        public UnityEvent OnEnter;
        public UnityEvent OnExit;

        public void OnTriggerEnter(Collider other)
        {
            if (!other.gameObject.CompareTag("Player"))
                return;

            if (DialogueUtil.AreAllSet(RequiredSetFlags) && DialogueUtil.AreAllUnset(RequiredUnsetFlags) && OnEnter != null)
                OnEnter.Invoke();
        }

        public void OnTriggerExit(Collider other)
        {
            if (!other.gameObject.CompareTag("Player"))
                return;

            if (DialogueUtil.AreAllSet(RequiredSetFlags) && DialogueUtil.AreAllUnset(RequiredUnsetFlags) && OnExit != null)
                OnExit.Invoke();
        }

        public void SetFlag(string flag) => DialogueUtil.SetFlag(flag);

        public void UnsetFlag(string flag) => DialogueUtil.UnsetFlag(flag);
    }
}