using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FillerNPC_Inn_Drinker : MonoBehaviour
{
    public GameObject BeerTablePos;
    public GameObject Self;
    public GameObject ItemHolder;
    public Transform BeerHandPos;
    public GameObject Beer;
    public Animator Animator;
    
    public float sleepMin = 1f;
    public float sleepMax = 6f;
    public float drinkAnimCorrectionStart = 0.3f;
    public float drinkAnimCorrectionEnd = 0.7f;

    private float timer;

    private AnimatorStateInfo animatorState;    
    
    // Start is called before the first frame update
    void Start()
    {
        //parent = GetComponent<GameObject>();
        ResetTimer();
    }

    // Update is called once per frame
    void Update()
    {
        animatorState = Animator.GetCurrentAnimatorStateInfo(0);
if(animatorState.IsName("Idle"))
{
    timer -= Time.deltaTime;
    //print(timer);
    if (timer <= 0.0f)
    {
         Drink();
         ResetTimer();
         return;
    }
    //
    PlaceBeerAtTable();
}
else
{
    if(animatorState.normalizedTime < drinkAnimCorrectionStart)
    {   return; }
    else if (animatorState.normalizedTime > drinkAnimCorrectionEnd)
    {
        PlaceBeerAtTable();
    }
    else
    {         
         Beer.transform.parent = ItemHolder.transform;
         Beer.transform.position = BeerHandPos.position;
    }

}
        
    }

    public void Drink()
    {
        Animator.SetTrigger("Drink");
    }

    public void PlaceBeerAtTable()
    {
        Beer.transform.parent = Self.transform;
        Beer.transform.position = BeerTablePos.transform.position;
        Beer.transform.rotation = BeerTablePos.transform.rotation;

    }

    public void ResetTimer()
    {
        timer = Random.Range(sleepMin, sleepMax);
    }
}
