﻿using UnityEngine;

namespace Nudle.Scripts
{
    public class Exit : MonoBehaviour
    {
        public Canvas Canvas;

        internal void Start() => Canvas.gameObject.SetActive(false);

        // Update is called once per frame
        internal void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
                Canvas.gameObject.SetActive(true);
        }

        public static void Quit() => Application.Quit();
    }
}