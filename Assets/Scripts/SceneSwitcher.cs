﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Nudle.Scripts
{
    public class SceneSwitcher : MonoBehaviour
    {
        public GameObject LoadingScreen;
        public string SceneToLoad;
        public Slider ProgressBar;
        public Text ProgressText;

        public Vector2 FloatingTextOffset = new Vector2(0f, 50f);

        private Text Floater;

        internal static string LastScene;

        public void LoadScene()
        {
            LastScene = SceneManager.GetActiveScene().name;
            StartCoroutine(LoadSceneCoroutine());
        }

        internal IEnumerator LoadSceneCoroutine()
        {
            var op = SceneManager.LoadSceneAsync(SceneToLoad);
            LoadingScreen.SetActive(true);

            while (!op.isDone)
            {
                var progress = Mathf.Clamp01(op.progress / .9f);
                ProgressText.text = $"{progress:P0}";
                ProgressBar.value = progress;

                yield return null;
            }
        }

        internal void OnTriggerEnter(Collider other)
        {
            if (!other.gameObject.CompareTag("Player"))
                return;
            if (Floater == null)
            {
                Floater = FindObjectOfType<FloatingTextManager>().ShowText("Press [E] to change location", other.transform, FloatingTextOffset);
            }
            other.gameObject.GetComponent<PlayerController>().OnIteract = () =>
            {
                if (Floater != null)
                {
                    Destroy(Floater.gameObject);
                    Floater = null;
                }
                LoadScene();
            };
        }

        internal void OnTriggerExit(Collider other)
        {
            if (!other.gameObject.CompareTag("Player"))
                return;
            if (Floater != null)
            {
                Destroy(Floater.gameObject);
                Floater = null;
            }
            other.gameObject.GetComponent<PlayerController>().OnIteract = null;
        }
    }
}