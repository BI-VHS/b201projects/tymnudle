﻿using UnityEngine;
using UnityEngine.UI;

namespace Nudle.Scripts
{
    public class FloatingTextSource : MonoBehaviour
    {
        public string Text;
        public Vector2 Offset;
        public Transform Source;

        public float Time;

        private Text textobj;

        public void Show() => textobj = FindObjectOfType<FloatingTextManager>().ShowText(Text, Source, Offset, Time);

        public void Hide() => Destroy(textobj);
    }
}