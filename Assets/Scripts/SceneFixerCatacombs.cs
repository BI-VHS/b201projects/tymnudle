﻿using UnityEngine;
using Nudle.Scripts.Dialogue;

namespace Nudle.Scripts
{
    public class SceneFixerCatacombs : MonoBehaviour
    {
        public TrailerDoor Door;
        public GameObject ViewBlockade;
        public GameObject Architect;
        public GameObject Skull;

        internal void Start()
        {
            if (DialogueUtil.IsFlagSet("Door_Opened"))
			{
                Door.Open();
				ViewBlockade.SetActive(false);
			}
			else
			{
				ViewBlockade.SetActive(true);
			}
        }

		public void DoorCheck()
		{
            if (DialogueUtil.IsFlagSet("Door_Opened"))
			{
                Door.Open();
				ViewBlockade.SetActive(false);
			}
		}

		public void ArchitectCheck()
		{
            if (DialogueUtil.IsFlagSet("Ghost_Found"))
			{
				Skull.SetActive(false);
				Architect.SetActive(true);
			}
		}
    }
}
