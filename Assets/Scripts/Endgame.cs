﻿using UnityEngine;

namespace Nudle.Scripts
{
    public class Endgame : MonoBehaviour
    {
		public StarWarsText StarWarsText_Sabotage;
		public StarWarsText StarWarsText_Combine;
		public StarWarsText StarWarsText_Fail;

        public void Show()
        {
            if (Dialogue.DialogueUtil.IsFlagSet("End_Game"))
			{
				if (Dialogue.DialogueUtil.IsFlagSet("Mayor_Dead") && Dialogue.DialogueUtil.IsFlagSet("Innkeeper_Convinced") && !Dialogue.DialogueUtil.IsFlagSet("Priest_Dead"))
				{
					StarWarsText_Combine.Show();
				}
				else if (Dialogue.DialogueUtil.IsFlagSet("Sabotage_Done"))
				{
					StarWarsText_Sabotage.Show();
				}
				else 
				{
					StarWarsText_Fail.Show();
				}
			}
		}
    }
}