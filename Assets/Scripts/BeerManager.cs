﻿using UnityEngine;
using System.Linq;
using Nudle.Scripts.AI;

namespace Nudle.Scripts
{
    public class BeerManager : MonoBehaviour
    {
        public BeerMaster BeerMaster;
        public GameObject[] Beers;

        public int AvailibleBeers() =>
            Beers.Count(beer => beer.activeSelf);

        public void RemoveBeer()
        {
            Beers.First(beer => beer.activeSelf).SetActive(false);
            BeerMaster.SendMessage("GrabBeers", SendMessageOptions.RequireReceiver);
        }

        public void PlaceBeers()
        {
            foreach (var beer in Beers)
                beer.SetActive(true);
        }
    }
}
