﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Nudle.Scripts.Dialogue
{
    public class Interactable : MonoBehaviour
    {
        //public Dialogue dialogue;
        public Dialogue[] PossibleDialogues;

        public string FloatingText = "Press [E] to interact";
        public Vector2 FloatingTextOffset = new Vector2(0f, 50f);
        public bool ForceStart;

        public UnityEvent OnStart;
        public UnityEvent OnExit;

        private Text Floater;

        private DialogueManager GetDialogueManager() => FindObjectOfType<DialogueManager>();

        public void TriggerDialogue()
        {
            if (GetDialogueManager().IsInDialogue())
                return;
            foreach (var possibleDialogue in PossibleDialogues)
            {
                if (DialogueUtil.AreAllSet(possibleDialogue.RequiredSetFlags) && DialogueUtil.AreAllUnset(possibleDialogue.RequiredUnsetFlags))
                {
                    var dm = GetDialogueManager();
                    dm.OnExit = OnExit;
                    dm.StartDialogue(possibleDialogue);
                    if (OnStart != null)
                        OnStart.Invoke();
                    return;
                }
            }
        }

        public void EndDialogue()
        {
            var dm = GetDialogueManager();
            dm.EndDialogue(null);
            dm.ClearChoices();
        }

        internal void OnTriggerEnter(Collider other)
        {
            if (!other.gameObject.CompareTag("Player"))
                return;

            if (ForceStart)
            {
                TriggerDialogue();
                return;
            }

            if (FloatingText != null && Floater == null)
            {
                Floater = FindObjectOfType<FloatingTextManager>().ShowText(FloatingText, gameObject.transform, FloatingTextOffset);
            }
            other.gameObject.GetComponent<PlayerController>().OnIteract = () => {
                if (Floater != null)
                {
                    Destroy(Floater.gameObject);
                    Floater = null;
                }
                TriggerDialogue();
            };
        }

        internal void OnTriggerExit(Collider other)
        {
            if (!other.gameObject.CompareTag("Player"))
                return;
            if (Floater != null)
            {
                Destroy(Floater.gameObject);
                Floater = null;
            }
            other.gameObject.GetComponent<PlayerController>().OnIteract = null;
        }
    }
}