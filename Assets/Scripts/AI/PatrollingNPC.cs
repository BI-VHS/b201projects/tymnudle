using System.Collections;
using UnityEngine;
using UnityEngine.AI;

namespace Nudle.Scripts.AI
{
    public class PatrollingNPC : MonoBehaviour
    {
        public Transform[] PathPoints;
        public float[] PathDelays; // horrible hack, but w/e
        public NavMeshAgent Agent;
        public Animator Animator;

        [Range(0f, 0.5f)]
        public float GoodEnough = 0.25f;

        [SerializeField]
        private int pathIndex;

        [SerializeField]
        private bool stopped;

        public void Pause()
        {
            Agent.isStopped = true;
            Animator.SetBool("isWalking", false);
        }

        public void Unpause()
        {
            if (stopped)
                return;

            Agent.isStopped = false;
            Animator.SetBool("isWalking", true);
        }

        internal void Start()
        {
            if (PathDelays.Length < PathPoints.Length)
            {
                var tmp = PathDelays;
                PathDelays = new float[PathPoints.Length];
                for (int i = 0; i < tmp.Length; i++)
                    PathDelays[i] = tmp[i];
            }
            Agent.SetDestination(PathPoints[0].position);
            Animator.SetBool("isWalking", true);
        }

        internal void Update()
        {
            if (stopped)
                return;
            if (Agent.remainingDistance <= 0.01f + GoodEnough)
            {
                var delay = PathDelays[pathIndex];
                if (delay > 0)
                    StartCoroutine(StopFor(delay));
                pathIndex = (pathIndex + 1) % PathPoints.Length;
                Agent.SetDestination(PathPoints[pathIndex].position);
            }
        }

        public IEnumerator StopFor(float seconds)
        {
            Agent.isStopped = true;
            stopped = true;
            Animator.SetBool("isWalking", false);
            yield return new WaitForSeconds(seconds);
            Agent.isStopped = false;
            stopped = false;
            Animator.SetBool("isWalking", true);
        }
    }
}