﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Nudle.Scripts
{
    public class StarWarsText : MonoBehaviour
    {
        public Canvas StarWarsCanvas;
        public Text TextBox;

        [TextArea(3, 10)]
        public string[] Text;

        public bool GameOver;

        private int idx;
        private bool active;

        internal void Start()
        {
            StarWarsCanvas.gameObject.SetActive(false);
        }

        internal void Update()
        {
            if (!active)
                return;

            if (Input.GetKeyDown(KeyCode.Space))
                DisplayNext();
        }

        public void Show()
        {
            StarWarsCanvas.gameObject.SetActive(true);
            active = true;
            idx = 0;
            DisplayNext();
            FindObjectOfType<PlayerController>().CanWalk = false;
        }

        public void Hide()
        {
            StarWarsCanvas.gameObject.SetActive(false);
            active = false;
            idx = 0;
            FindObjectOfType<PlayerController>().CanWalk = true;
            if (GameOver)
            {
                Dialogue.DialogueUtil.ClearFlags();
                SceneSwitcher.LastScene = null;
                SceneManager.LoadScene("les_novy");
            }
        }

        internal void DisplayNext()
        {
            if (idx >= Text.Length)
            {
                Hide();
                return;
            }
            TextBox.text = Text[idx++];
        }
    }
}