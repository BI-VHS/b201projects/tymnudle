﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using UnityEngine.Events;

namespace Nudle.Scripts.Dialogue
{
    public class DialogueManager : MonoBehaviour
    {
        public Text NameText;
        public Text DialogueText;
        public Button ChoiceButtonPrefab;
        public Canvas DialogueCanvas;

        public Animator animator;

        private Queue<string> lines;
        private Dialogue currentDialogue;
        private List<Button> choiceButtons;

        internal UnityEvent OnExit;

        internal void Start()
        {
            choiceButtons = new List<Button>();
            lines = new Queue<string>();
            DialogueCanvas.gameObject.SetActive(true);
        }

        internal void Update()
        {
            if (IsInDialogue() && Input.GetKeyDown(KeyCode.Space))
                DisplayNextLine();
        }

        public bool IsInDialogue() => !(currentDialogue is null);

        public void StartDialogue(Dialogue dialogue)
        {
            currentDialogue = dialogue;

            animator.SetBool("IsOpen", true);

            NameText.text = dialogue.CharacterName;

            lines.Clear();

            foreach (var line in dialogue.Lines)
            {
                lines.Enqueue(line);
            }

            DisplayNextLine();
        }

        public void DisplayNextLine()
        {
            if (lines.Count == 0)
            {
                DisplayChoices();
                return;
            }
            DialogueText.text = lines.Dequeue();
        }

        public void DisplayChoices()
        {
            if (choiceButtons.Count > 0 || currentDialogue is null)
                return;
            if (currentDialogue.Choices is null || currentDialogue.Choices.Length == 0 || !currentDialogue.Choices.Any(choice => IsValid(choice)))
            {
                EndDialogue(currentDialogue.NextDialogue);
                return;
            }

            var validChoices = currentDialogue.Choices.Where(choice => IsValid(choice));

            var count = validChoices.Count();
            var i = 0;
            foreach (var choice in validChoices)
            {
                var button = Instantiate(ChoiceButtonPrefab);
                choiceButtons.Add(button);
                button.transform.SetParent(DialogueCanvas.transform);
                button.transform.GetChild(0).GetComponent<Text>().text = choice.Text;
                button.transform.localScale = Vector3.one;
                button.onClick.AddListener(() => SelectChoice(choice));
                button.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, (i * 100) - 100 + ((5 -count) * 50));
                ++i;
            }
        }

        private bool IsValid(Choice choice)
        {
            var hasAllFlags = true;
            foreach (var flag in choice.RequiredSetFlags)
                hasAllFlags &= DialogueUtil.IsFlagSet(flag);
            foreach (var flag in choice.RequiredUnsetFlags)
                hasAllFlags &= !DialogueUtil.IsFlagSet(flag);
            return hasAllFlags;
        }

        public void SelectChoice(Choice choice)
        {
            ClearChoices();
            EndDialogue(choice.NextDialogue);
        }

        public void EndDialogue(Dialogue next)
        {
            foreach (var flag in currentDialogue.SetFlags)
                DialogueUtil.SetFlag(flag);
            foreach (var flag in currentDialogue.UnsetFlags)
                DialogueUtil.UnsetFlag(flag);

            if (next is null)
            {
                animator.SetBool("IsOpen", false);
                currentDialogue = null;
                if (OnExit != null)
                    OnExit.Invoke();
                OnExit = null;
                return;
            }
            StartDialogue(next);
        }

        public void ClearChoices()
        {
            choiceButtons.ForEach(button => Destroy(button.gameObject));
            choiceButtons.Clear();
        }
    }
}