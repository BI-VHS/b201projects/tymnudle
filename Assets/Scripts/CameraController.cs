﻿using System.Collections.Generic;
using UnityEngine;

namespace Nudle.Scripts
{
    public class CameraController : MonoBehaviour
    {
        public Camera Camera;
        public GameObject Focus;
        public float CameraDistance = 4.5f;
        public float CameraAngle = 0f;
        public Vector3 FocusOffset = Vector3.zero;

        public float MaxCameraDistance = 10f;
        public float MinCameraDistance = 3f;
        public float MaxCameraAngle = 70f;
        public float MinCameraAngle = 40f;

        public float CameraRotateSpeed = 1500f;
        public float CameraZoomSpeed = 20f;

        public LayerMask ObstructionMask;
        private List<Transform> _LastTransforms;

        internal void Start() => _LastTransforms = new List<Transform> ( );

        internal void Update() => MoveCamera ( );

        internal void LateUpdate() => HideObstructions ( );

        private void MoveCamera()
        {
            if ( Input.GetMouseButton ( 1 ) )
            {
                var mouseY = Input.GetAxis ( "Mouse Y" );

                CameraAngle += mouseY * CameraRotateSpeed * Time.deltaTime;
                CameraAngle = Mathf.Clamp ( CameraAngle, MinCameraAngle, MaxCameraAngle );
            }

            var direction = 0;
            if ( Input.GetKey ( KeyCode.LeftBracket ) )
                direction--;
            if ( Input.GetKey ( KeyCode.RightBracket ) )
                direction++;

            CameraDistance = Mathf.Clamp ( CameraDistance + ( direction * CameraZoomSpeed * Time.deltaTime ), MinCameraDistance, MaxCameraDistance );

            transform.position = Focus.transform.position + FocusOffset + ( Quaternion.Euler ( -CameraAngle, 180f, 0f ) * ( CameraDistance * -Vector3.back ) );
            transform.LookAt ( Focus.transform.position + FocusOffset, Vector3.up );
        }

        private void HideObstructions()
        {
            if ( _LastTransforms.Count > 0 )
            {
                foreach ( Transform t in _LastTransforms )
                    t.GetComponent<MeshRenderer> ( ).shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
                _LastTransforms.Clear ( );
            }

            //Cast a ray from this object's transform the the watch target's transform.
            RaycastHit[] hits = Physics.RaycastAll (
              transform.position,
              Focus.transform.position - transform.position,
              Vector3.Distance ( Focus.transform.position, transform.position ),
              ObstructionMask
            );

            //Loop through all overlapping objects and disable their mesh renderer
            if ( hits.Length > 0 )
            {
                foreach ( RaycastHit hit in hits )
                {
                    if ( hit.collider.gameObject.transform != Focus && hit.collider.transform.root != Focus )
                    {
                        hit.collider.gameObject.GetComponent<MeshRenderer> ( ).shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.ShadowsOnly;
                        _LastTransforms.Add ( hit.collider.gameObject.transform );
                    }
                }
            }
        }
    }
}