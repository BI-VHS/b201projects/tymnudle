﻿using UnityEngine;

namespace Nudle.Scripts
{
    public class EndgameInn : MonoBehaviour
    {
		public StarWarsText StarWarsText_Innkeeper_Murderer;
		public StarWarsText StarWarsText_Innkeeper;

        public void Show()
        {
			if (Dialogue.DialogueUtil.IsFlagSet("Innkeeper_Dead"))
			{
				if (Dialogue.DialogueUtil.IsFlagSet("Mayor_Dead")
					|| Dialogue.DialogueUtil.IsFlagSet("Scribe_Dead")
				  	|| Dialogue.DialogueUtil.IsFlagSet("Witch_Dead"))
				{
            		StarWarsText_Innkeeper_Murderer.Show();
				}
				else
				{
            		StarWarsText_Innkeeper.Show();
				}
			}
        }
    }
}