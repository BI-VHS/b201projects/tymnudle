﻿using UnityEngine;

namespace Nudle.Scripts
{
    public class EndgameSquare : MonoBehaviour
    {
		public StarWarsText StarWarsText_Captain;
		public StarWarsText StarWarsText_Captain_Murderer;
		public StarWarsText StarWarsText_Priest;
		public StarWarsText StarWarsText_Priest_Murderer;
		public StarWarsText StarWarsText_Priest_Pyrrha;

        public void ShowPriest()
        {
            if (Dialogue.DialogueUtil.IsFlagSet("Priest_Dead"))
			{
				if(Dialogue.DialogueUtil.IsFlagSet("Mayor_Dead") && Dialogue.DialogueUtil.IsFlagSet("Innkeeper_Convinced"))
				{
            		StarWarsText_Priest_Pyrrha.Show();
				}
				else if (Dialogue.DialogueUtil.IsFlagSet("Mayor_Dead")
					|| Dialogue.DialogueUtil.IsFlagSet("Scribe_Dead")
				  	|| Dialogue.DialogueUtil.IsFlagSet("Witch_Dead"))
				{
            		StarWarsText_Priest_Murderer.Show();
				}
				else
				{
            		StarWarsText_Priest.Show();
				}
			}
        }

        public void ShowCaptain()
        {
            if (Dialogue.DialogueUtil.IsFlagSet("Captain_Dead"))
			{
				if (Dialogue.DialogueUtil.IsFlagSet("Mayor_Dead")
					|| Dialogue.DialogueUtil.IsFlagSet("Scribe_Dead")
				  	|| Dialogue.DialogueUtil.IsFlagSet("Witch_Dead"))
				{
            		StarWarsText_Captain_Murderer.Show();
				}
				else
				{
            		StarWarsText_Captain.Show();
				}
			}
        }
    }
}