using UnityEngine;
using Nudle.Scripts.Dialogue;

namespace Nudle.Scripts
{
    public class SceneFixerSquare : MonoBehaviour
    {
        public GameObject DungeonAccess;
        public GameObject Witch;
        public GameObject WitchTrigger;
        public GameObject PotionBuyer;
		public GameObject Dungeon_Examin;

        internal void Start()
        {
            if (!DialogueUtil.IsFlagSet("Dungeon_Access"))
                SetDungeonLock(true);
            if (DialogueUtil.IsFlagSet("Witch_Dead"))
                KillWitch();
        }

		public void CheckDungeon()
		{
            if (!DialogueUtil.IsFlagSet("Dungeon_Access"))
			{
                SetDungeonLock(true);
			}
			else
			{
				SetDungeonLock(false);
				Dungeon_Examin.SetActive(false);
			}
		}

        public void KillWitch()
        {
            if (!DialogueUtil.IsFlagSet("Witch_Dead"))
                return;
            Witch.GetComponent<Animator>().enabled = false;
            WitchTrigger.SetActive(false);         
            PotionBuyer.SetActive(false);
        }

        public void SetDungeonLock(bool locked) => DungeonAccess.SetActive(!locked);
    }
}
