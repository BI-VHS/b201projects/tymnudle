# Statický Svět

[zpět na celou dokumentaci](README.md)

## Scény

- Les
- Náměstí
- Hospoda
- Radnice
- Podzemí

### Les
Úvodní lokace hry která slouží jako startovní bod a také obsahuje první stopy pro hráče.
Jedná se o malý kus lesa ohraničený stromy, cestou a srázem.

<img width="700" alt="les" src="Assets/Screenshots/Les.png">

Na cestě jsou boží muka které slouží jako orientační bod, u kterého začíná lesní cesta k potenciálně magickému incidentu, který má vyšetřit.

<img width="700" alt="bozi_muka" src="Assets/Screenshots/Les_BoziMuka.png">

Uprostřed lesa je mrtvá oblast, stopa po magickém rituálu, který sem jde hráč vyšetřit.

<img width="700" alt="korupce" src="Assets/Screenshots/Les_Korupce.png">

### Náměstí
Největší lokace hry. Město, ve kterém se odehrává naše vyšetřování. Slouží k propojení ostatních scén. Bude tam několik lidí, stráže, žebrák, kněz, čarodějka a kapitán stráží.

<img width="700" alt="mesto" src="Assets/Screenshots/Mesto_OverView.png">

### Hospoda
Jedna místnost s hromadou stolů a pultem která slouží jako lokace pro hospodského a barda/ku.

<img width="700" alt="hospoda" src="Assets/Screenshots/Hospoda.png">

### Radnice
Dvě vedlejší místnosti které slouží jako lokace pro starostu a písaře.

<img width="700" alt="radnice" src="Assets/Screenshots/Radnice_OverView.png">

První je starostova kancelář se stolem s dokumenty.

<img width="700" alt="starosta" src="Assets/Screenshots/Radnice_Starosta.png">

Druhá je písařova místnost s knihovnami a stolem s knihami.

<img width="700" alt="knihovna" src="Assets/Screenshots/Radnice_Knihovna.png">

Písař tam má na stole hromadu knih aby vytvářeli pocit, že tam má rozdělanou práci.

<img width="700" alt="stul_pisare" src="Assets/Screenshots/Radnice_knihovna_stul.png">


### Podzemí
Místo vyvrcholení hlavní dějové linky. Lokace pro kultisty a rituál

<img width="700" alt="stul_pisare" src="Assets/Screenshots/Podzemi_OverView.png">

Oltář je místo rituálu, který je myšlen jako závěr příběhu.

<img width="700" alt="stul_pisare" src="Assets/Screenshots/Podzemi_Oltar.png">

Na stole leží rozložené materiály potřebné k provedení rituálu.

<img width="700" alt="stul_pisare" src="Assets/Screenshots/Podzemi_Stul.png">

## Objekty
Pro projekt byli vytvořeny originální modely.

### Postavy

- magický detektiv "Ohař" (hlavní postava)

### Les

- živý strom
- mrtvý strom
- keř
- zídky
- boží muka

### Náměstí

- kostel
- domek 1

### Hospoda

TBD

### Radnice

TBD

### Podzemí

TBD

## Souhrn

- 5 scén
- model hlavní postavy, stromy, blabla
- mnoho unikátních objektů jako boží muka, zídky a další