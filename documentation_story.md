# Design Příběhu

Jelikož celý koncept vznikl z myšlenky magického detektiva jak řeší nějaký případ, bylo jasné že tento případ bude mnohem důležitější než jakékoliv mechaniky.
To znamená že z důvodu omezených časových zdrojů, místo mechanik bude pouze příběh.

## Základní premisa

Hlavní postava je magický detektiv, který jde vyšetřit magickou anomálii a zajistit utajení existence magie.
Tato anomálie byla způsobena místním kultem který tam zkoušel rituál oživení mrtvých na kočce.
Kult má následně v plánu oživit člověka.

### Za premisou

Původní plán byl jednoduchý. Detektiv co jde vyšetřovat kult do města.
Hlavní bylo aby nějak dávalo smysl co chce detektiv a co chtějí kultiské.
Tak vznikl detektivův cíl "Zajistit utajenost magie" s tím že jeho motivace je jednoduchá, je to jeho práce a byl tam poslán vykonat.
Kultisté tím pádem bylo třeba udělat protichůdné, aby šli proti cílu hlavní postavy.
To znamená že ať dělají cokoliv, musí být výsledek každopádně, ať cíleně nebo náhodou, způsobit odhalení magie.
V našem případě jsme došli k tomu, že se snaží oživit veřejně známou osobu, což také zabije většinu lidí ve městě.

## Nelinearita

Jelikož příběh se stala hlavní mechanikou, chtěli jsme udělat příběh nelineárním.
A to ve smyslu, že nebude jasná cesta ke konci.

<img width="500" alt="endings" src="Assets/Screenshots/Arcweave_1.png">

Zde je jedna ze starších verzí diagramu použitého při vytváření příběhu.

### Stopy

Příběh se pohybuje skrze stopy. Střípky informací které postupně vedou ke konci příběhu.
Tyto střípky se pohybují především skrze dialogy s postavami.
Pokud má hráč určitou informaci, tak může získat od určité osoby na základě toho novou.

#### Jeden na všechny, všichni na jednoho

Důležitou částí nelinearity bylo aby byli stopy, které jde získat více způsoby, jako například lokace kde závěrečný rituál kult provede.
Zároveň tam musí být i informace, která potřebuje kombinaci stop, že společně odhalí další informace.
Kvůli omezeného rozsahu, k tomu aby bylo reálné hru dokončit, tam obojího je méně než by jsme si přáli, ale mají vždy alespoň jednu instanci.

## Konce

Původně měly všechny konce vést k jednomu specifickému vyvrcholení, ale v průběhu tvorby se to mírně změnilo.
Jak vznikaly jednotlivé stopy a příběh za nimi, vznikaly také uvědomění si že pak dává smysl i alternativní způsob jak to zakončit.
A tak postupně vznikaly různé konce, které závisí na tom jak hráč provede své vyšetřování. A to dokonce do takové míry, že původní nápad na konec se kompletně zahodil.

<img width="350" alt="endings" src="Assets/Screenshots/Arcweave_2.png">

Výsledek je více méně sedm konců. Ale některé z nich jsou kombinací jiných a některé jsou prosně neúspěšné varianty konců.
Reálnější je brát je jakožto 3 unikátní konce a ty zbylé jsou variace na ně.

### Konce do hloubky: konfrontace

První konec je přijít na všechny tři kultisty a zbavit se jich. To je možno skrze konfrontace, při kterých jde zabít podezřelé (možnost konfrontace není pouze u pachatelů ale i u ostatních podezřelých, aby se tím nedávala nápověda hráči).
Problém je, že dva ze tří kultistů jsou na veřejném místě a jejich odstranění způsobí upozornění stráží a zatčení hlavní postavy. Je ale způsob jak to stejně vyřešit.

### Konce do hloubky: sabotace

Další možnost je najít lokaci rituálu a sabotovat ho aby to zabilo všechny zůčastněné ale při tom selhal. Zde to je trochu čistší výsledek, ale i ten má svou komplikaci. Nyní ne technickou, ale spíše mírně morální.

### Konce do hloubky: domluva

Poslední konec je nejnovější. A to jest kultisty přesvědčit. Zde o tom nebudu nic moc říkat, nežli že to jde.

## Dobrý nápad, ale co třeba ne

Hodně nápadů při tvorbě příběhu bylo, ale dost jich bylo zavrhnuto. Některé kvůli tomu že nebyli dostatečně dobré, ale bohužel mnoho bylo zavrhnuto z prostého důvodu, a to jsou zdroje. Nejvíce šlo o zdroj časový. Je to semestrální projekt a tak nemáme moc času na provedení všech našich nápadů.
A tak proběhlo škrtání. Některé kvůli tomu, že bychom museli přidávat celé nové mechaniky. Další že by protahovali příběh. A některé protože nebyli vhodné ke zbytku hry. Zde je jedna z nich, protože si zaslouží být zmíněna.

### Delší podpříběh s čarodejnicí

Čarodejnice má silný názor na doposud dost nejasnou Radu (Council) pro kterou pracuje hlavní postava. To jsme chtěli využít k tomu že by jednání s ní muselo probýhat trochu opatrněji a to kdy odhalíš že pro ně pracuješ bude mít efekt na to jak bude spolupracovat. Kde utajování z počátku umožní spolupráci čarodejnice, ale čím déle to bude hráč tajit, tím horší bude její reakce když se to dozví, což by se s jistotou dříve či později stalo. Když by ale to odhalil z počátku, tak by nebyla moc nápomocná, ale skrze upřímnost pak bude ochotna plně pomoct v závěru, kdy to bude nejdůležitější.
Problém byl, že to by potřebovalo o dost delší příběh, než nakonec máme. Plus dosavadní systém dialogů neumožňuje více skriptované aspekty, jako třeba počítání nějakého druhu událostí (utajování identity). Nakonec bylo vyhodnoceno, že to není nutný aspekt a tak to bylo odebráno.