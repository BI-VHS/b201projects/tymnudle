using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScribeRagdollFix : MonoBehaviour
{

    public Animator Animator;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(fixThisFreakyThingHopefully());
    }

    IEnumerator fixThisFreakyThingHopefully()
    {
        Animator.enabled = false;
        yield return new WaitForSeconds(0.5f);
        Animator.enabled = true;
    }
}
