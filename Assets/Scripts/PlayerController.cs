﻿using Nudle.Scripts.Dialogue;
using System;
using System.Text;
using UnityEngine;

namespace Nudle.Scripts
{
    public class PlayerController : MonoBehaviour
    {
        public CharacterController CharacterController;
        public Animator Animator;
        public float MovementSpeed = 2f;
        public float SprintSpeed = 4f;

        internal bool CanWalk = true;

        private const float gravity = -9.81f;
        private bool isSprinting = false;

        [SerializeField]
        private float verticalVel;

        internal Action OnIteract;
        private DialogueManager _dialogueManager;

#if UNITY_EDITOR_WIN
        private readonly StringBuilder sb = new StringBuilder(1024);
#endif

        private DialogueManager DialogueManager
        {
            get
            {
                _dialogueManager = _dialogueManager != null ? _dialogueManager : FindObjectOfType<DialogueManager>();
                return _dialogueManager;
            }
        }

        private bool IsInDialogue() => DialogueManager != null && DialogueManager.IsInDialogue();

        internal void Start()
        {
            var lastScene = SceneSwitcher.LastScene;
            if (lastScene == null)
                return;
            Debug.Log(lastScene);
            var tf = GameObject.Find($"From_{lastScene}").transform;
            transform.position = tf.position;
            transform.rotation = tf.rotation;
        }

        internal void Update()
        {
            DoMove();
            DoInteract();
#if UNITY_EDITOR
            if (Input.GetKeyDown(KeyCode.F1))
            {
                sb.Clear();
                foreach (var flag in DialogueUtil.GetFlags())
                    sb.Append(flag).Append(", ");
                Debug.Log(sb.ToString());
            }
#endif
        }

        private void DoInteract()
        {
            if (Input.GetKeyDown(KeyCode.E) && OnIteract != null)
                OnIteract.Invoke();
        }

        private void DoMove()
        {
            var sMove = Input.GetAxis("Horizontal");
            var fMove = Input.GetAxis("Vertical");

            var input = new Vector3(sMove, 0, fMove).normalized;

            if (IsInDialogue() || !CanWalk)
                input = Vector3.zero;

            verticalVel = CharacterController.isGrounded ? 0 : verticalVel + (gravity * Time.deltaTime);

            if (Input.GetKey(KeyCode.LeftShift))
            {
                isSprinting = input.magnitude > 0f;
                Animator.SetBool("isSprinting", isSprinting);
                Animator.SetBool("isWalking", false);
                transform.rotation = Quaternion.LookRotation(Vector3.RotateTowards(transform.forward, input, 10f * Time.deltaTime, 0.0F));
                CharacterController.Move(((transform.forward * SprintSpeed * input.magnitude) + new Vector3(0, verticalVel, 0)) * Time.deltaTime);
            }
            else
            {
                Animator.SetBool("isSprinting", false);
                Animator.SetBool("isWalking", input.magnitude > 0f);
                transform.rotation = Quaternion.LookRotation(Vector3.RotateTowards(transform.forward, input, 10f * Time.deltaTime, 0.0F));
                CharacterController.Move(((transform.forward * MovementSpeed * input.magnitude) + new Vector3(0, verticalVel, 0)) * Time.deltaTime);
            }
        }
    }
}