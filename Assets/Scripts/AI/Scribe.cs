﻿using UnityEngine;
using UnityEngine.AI;

namespace Nudle.Scripts.AI
{
    public class Scribe : MonoBehaviour
    {
        public Transform[] Bookshelves;
        public Transform Seat;
        public NavMeshAgent Agent;
        public Animator Animator;

        private Transform lastBookshelf;
        private float Timer;

        [SerializeField]
        private AIState State;

        private AIState StateAfterWait;
        private Vector3 DestinationAfterWait;

        private bool paused;
        private bool shouldWalk;

        public void Pause()
        {
            paused = true;
            shouldWalk = Animator.GetBool("isWalking");
            Animator.SetBool("isWalking", false);
        }

        public void Unpause()
        {
            paused = false;
            Animator.SetBool("isWalking", shouldWalk);
        }

        internal void Start()
        {
            State = AIState.READING;
            lastBookshelf = GetRandomBookshelf();
            Timer = 1f;
        }

        internal void Update()
        {
            if (paused)
                return;

            switch (State)
            {
                case AIState.READING:
                    Reading();
                    break;

                case AIState.RETURNING_BOOK:
                    ReturningBook();
                    break;

                case AIState.GRABBING_BOOK:
                    GrabbingBook();
                    break;

                case AIState.RETURNING_TO_SEAT:
                    ReturningToSeat();
                    break;

                case AIState.WAITING:
                    Waiting();
                    break;
            }
        }

        private void Reading()
        {
            Timer -= Time.deltaTime;
            if (Timer <= 0f)
            {
                State = AIState.RETURNING_BOOK;
                Agent.SetDestination(lastBookshelf.position);
                Animator.SetBool("isWalking", true);
            }
        }

        private void ReturningBook()
        {
            if (Agent.remainingDistance <= 0.01f)
            {
                StateAfterWait = AIState.GRABBING_BOOK;
                lastBookshelf = GetRandomBookshelf();
                DestinationAfterWait = lastBookshelf.position;
                State = AIState.WAITING;
                Animator.SetBool("isWalking", false);
                Timer = 1f + Random.Range(0f, 0.5f);
            }
        }

        private void GrabbingBook()
        {
            if (Agent.remainingDistance <= 0.01f)
            {
                StateAfterWait = AIState.RETURNING_TO_SEAT;
                DestinationAfterWait = Seat.position;
                State = AIState.WAITING;
                Animator.SetBool("isWalking", false);
                Timer = 1f + Random.Range(0f, 0.5f);
            }
        }

        private void ReturningToSeat()
        {
            if (Agent.remainingDistance <= 0.01f)
            {
                State = AIState.READING;
                Animator.SetBool("isWalking", false);
                Timer = 5f + Random.Range(1f, 10f);
            }
        }

        private void Waiting()
        {
            Timer -= Time.deltaTime;
            if (Timer <= 0f)
            {
                State = StateAfterWait;
                Agent.SetDestination(DestinationAfterWait);
                Animator.SetBool("isWalking", true);
            }
        }

        private Transform GetRandomBookshelf() => Bookshelves[Random.Range(0, Bookshelves.Length)];

        private enum AIState
        {
            READING,
            RETURNING_BOOK,
            GRABBING_BOOK,
            RETURNING_TO_SEAT,
            WAITING,
        }

        internal void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawLine(transform.position, Agent.destination);
            if (lastBookshelf != null)
            {
                Gizmos.color = Color.green;
                Gizmos.DrawLine(transform.position, lastBookshelf.position);
            }
        }
    }
}