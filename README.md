# Nudle - The Hound

Adventura z prostředí středověkého světa s tajným magickým světem který s ním soužije.
Hráč bude následovat příběh magického detektiva poslaného do malého města vyšetřit potenciální magický jev a jaký je jeho zdroj a následně zajistit ať se neopakuje.

![motivační tematický obrázek který bude doplněn později](motivation.png)

Hru ovládá hráč zkrze konverzace s postavama nebo prozkoumáváním objektů.
Nemá to přímo lineární postup ale spíš je tam síť stop po které se může několika způsoby dostat k tomu co se děje.

## Design Postav

[Detailní popis rozhodnutí při designu postav](Character design - Nudle.adoc)

## Statický svět

[První část dokumentace (statická)](documentation_static.md)

## Dynamický svět

[Druhá část dokumentace (dynamcká)](documentation_dynamic.md)

## Complexní svět

[Třetí část dokumentace (komplexní)](documentation_complex.md)

## Trailer

[The Hound - Trailer (YouTube)](https://youtu.be/hvCnWZj7ex0)