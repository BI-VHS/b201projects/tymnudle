﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Nudle.Scripts
{
    public class FloatingTextManager : MonoBehaviour
    {
        public Canvas FloatingTextCanvas;
        public Text TextPrefab;

        internal Text ShowText(string text, Transform source, Vector2 offset, float time = -1f)
        {
            var uiText = Instantiate(TextPrefab);
            uiText.transform.SetParent(FloatingTextCanvas.transform);
            uiText.text = text;
            uiText.transform.localScale = Vector3.one;
            var floatingText = uiText.gameObject.GetComponent<FloatingText>();
            floatingText.Source = source;
            floatingText.Offset = offset;
            if (time > 0f)
                StartCoroutine(Util.DeleteAfter(uiText.gameObject, time));
            return uiText;
        }

        internal void ShowText(string text, Transform source, float time = -1f) => ShowText(text, source, Vector2.zero, time);
    }
}
