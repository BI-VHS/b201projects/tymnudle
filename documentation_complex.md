# Koplexní svět

[zpět na celou dokumentaci](README.md)

## Jednoduché dialogy a interakce s prostředím

Je možnos interagovat s NPC postavama a některými objekty v prostředí.

Pokud jde o osobu, tak se zastaví a otočí směrem k hráči.

Skrze tyto dialogy/interakce se posouvá v [příběhu](documentation_story.md).

## Komplexní dialogy

U některých dialogů/interakcí jsou možnosti volby. Těch bývá 1-5 a umožňují upravovat směr kterým konverzace směřuje.

## Postupování v příběhu

Konverzace se navzájem mohou ovlivňovat, kde jedna konverzace u osoby A zpřístupní novou dialogovou možnost u osoby B na základě nových informací.

<img width="350" alt="bardka o kočce" src="Assets/Screenshots/Complex/Cat_Bard.png">

<img width="350" alt="hospodský o kočce" src="Assets/Screenshots/Complex/Cat_Innkeeper.png">

### Nelineárnost

Příběh nemá lineární postupování, místo toho je složen z informací které zpřístupňují další části příběhu.

<img width="350" alt="mrtvá srna" src="Assets/Screenshots/Complex/Dead_Deer.png">

<img width="350" alt="srna u čarodějky" src="Assets/Screenshots/Complex/Dead_Deer_Witch.png">

<img width="350" alt="srna u písaře" src="Assets/Screenshots/Complex/Dead_Deer_Scribe.png">

### Důležitá rozhodnutí

Některé možnosti čistě posunují v příběhu a nijak nemění konečný závěr, ale jiné výsledný konec příběhu radikálně mění.

<img width="350" alt="rozhodnuti zabiti" src="Assets/Screenshots/Complex/Kill_Witch.png">

## Změna scén

Všechny scény jsou propojeny s náměstím a přesouvá se mezi nimi pomocí interakce se specifickými objekty.

### Les
Západní cesta - Brána

<img width="350" alt="cesta" src="Assets/Screenshots/Complex/Les_Cesta.png">

<img width="350" alt="brána" src="Assets/Screenshots/Complex/Mesto_Brana.png">

### Hospoda

Dveře - Vstup pod štítem hospody

<img width="350" alt="hospoda vystup" src="Assets/Screenshots/Complex/Hospoda_Vystup.png">

<img width="350" alt="hospoda vstup" src="Assets/Screenshots/Complex/Hospoda_Vstup.png">

### Radnice

Dveře - Vstup pod štítem radnice

<img width="350" alt="radnice vystup" src="Assets/Screenshots/Complex/Radnice_Vystup.png">

<img width="350" alt="radnice vstup" src="Assets/Screenshots/Complex/Radnice_Vstup.png">

### Podzemí

Dveře - Vstup do kostela na hřbitově

<img width="350" alt="kostnice vystup" src="Assets/Screenshots/Complex/Dungeon_Vystup.png">

<img width="350" alt="kostnice vstup" src="Assets/Screenshots/Complex/Dungeon_Vstup.png">

