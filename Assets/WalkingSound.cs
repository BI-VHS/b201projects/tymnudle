﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkingSound : MonoBehaviour
{
    [SerializeField]
	private AudioClip[] clips;

	[SerializeField]
	private AudioSource audioSource;

	private void Step()
	{
		AudioClip clip = GetRandomClip();
		audioSource.PlayOneShot(clip);
	}

	private AudioClip GetRandomClip()
	{
		return clips[UnityEngine.Random.Range(0, clips.Length)];
	}
}
