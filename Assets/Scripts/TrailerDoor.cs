﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailerDoor : MonoBehaviour
{
    //public float delay = 3.5f;
    public float speed = 1.5f;
    public Transform leftHinge;
    Quaternion leftOpened = Quaternion.Euler(0, 135, 0);
    Quaternion leftClosed = Quaternion.Euler(0, 135 - 90, 0);
    public Transform rightHinge;
    Quaternion rightOpened = Quaternion.Euler(0, 360 + 90 - 135, 0);
    Quaternion rightClosed = Quaternion.Euler(0, 360 + 90 - 135 + 90, 0);

    Quaternion rightTarget = Quaternion.Euler(0, 360 + 90 - 135 + 90, 0);
    Quaternion leftTarget = Quaternion.Euler(0, 135 - 90, 0);

    //float smooth = 5.0f;
    //float tiltAngle = 60.0f;

	public void Open()
	{
		leftTarget = leftOpened;
		rightTarget = rightOpened;
	}

	public void Close()
	{
		leftTarget = leftClosed;
		rightTarget = rightClosed;
	}

    void Update()
    {
        //if (delay > 0)
        //    delay -= Time.deltaTime;
        //else
        //{
            //float tiltAroundX = Input.GetAxis("Vertical") * tiltAngle;

            leftHinge.rotation = Quaternion.Slerp(leftHinge.rotation, leftTarget, Time.deltaTime * speed);// smooth);
            rightHinge.rotation = Quaternion.Slerp(rightHinge.rotation, rightTarget, Time.deltaTime * speed);// smooth);

            /*
            float rotx = leftHinge.rotation.x + speed * Time.deltaTime;
            if (rotx > 90) rotx = 90;
            leftHinge.rotation = new Quaternion(rotx, leftHinge.rotation.y, leftHinge.rotation.z, leftHinge.rotation.w);

            rotx = rightHinge.rotation.x - speed * Time.deltaTime;
            if (rotx < -90) rotx = -90;
            rightHinge.rotation = new Quaternion(rotx, rightHinge.rotation.y, rightHinge.rotation.z, rightHinge.rotation.w);
            */
        //}
    }
}
