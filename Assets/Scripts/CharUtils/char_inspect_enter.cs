using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class char_inspect_enter : MonoBehaviour
{
    public Animator playerAnimator;
    
    public void Start()
    {
        playerAnimator.SetBool("isInspecting", true);
    }
    
    public void Update()
    {

    }
    public void Execute()
    {
        playerAnimator.SetBool("isInspecting", true);
    }
}

