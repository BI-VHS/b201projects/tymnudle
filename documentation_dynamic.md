# Dynamický Svět

[zpět na celou dokumentaci](README.md)

## Animace

Všechny postavy mají obecné animace chůze, běhu a sezení.

Pro specifické instance jsou také vytvořeny unikátní animace mezi které patří:

*  Žebrák: žebrání v kleku
  
### Originální postavy

Všechny postavy včetně animací byli [nadesignované](Character design - Nudle.adoc) a vytvořené speciálně pro tento projekt.

## Cyklické činnosti

### Knihovna

Písař pravidelně čte knihy.
Tyto knihy bere z jednotlivých knihoven a po přečtení ji vrátí do stejné knihovny ze které ji původně sebral.

Vezme si knihu z knihovny -> Jde si sednout -> Sedí a čte -> Jde uklidí knihu zpátky -> Jde si vzít novou knihu

### Náměstí

Na náměstí, aby působilo živě, budou lidé chodit od domů ke stánkům, kde po navštívení několika stánků se vrátí zpět do svého domu.

Dům -> Stánky -> Dům

### Hospoda

V hospodě hosté chodí pro pivo k baru a pak ho jdou vypít u stolu.

Hosti: Stůl -> Pult -> Stůl

Kočka bude střídat dva stavy, jeden kde leží na místě a spí a druhý kde se prochází po hospodě.

Kočka: Spí -> Prochází se -> Spí

## Produkce

### Hospoda

Pivo v hospodě hostinský nabírá u sudů a pak ho pokládá na pult, kam si pro něj chodí hosté.

Hospodský:Sud -> Pult -> Patron:Stůl

### Náměstí

Měšťané při svém procházení se po městě budou mít šanci, že u nějakého stánku něco koupí.
Prodejci to následně musí doplnit ze svých zásob v krabici.

Krabice -> Stánek -> Zákazník

## Chování

### Hospoda

Když se mluví s bardkou, tak přestane hrát hudba.

### Les

Králík se bude pohybovat po lese. Když se hráč dostane do určitý vzdálenosti, tak uteče do nejvhodnější nory (schované v křoví) směrem od hráče.

### Náměstí

Když hráč zabije postavu na veřejném místě, stráže na náměstí ho obklíčí a pokusí se ho zatknout.

## Unikáty

### Les

Králík, když nenajde vhodný keř kam se schovat řekne "I don't vibe with this universe" a následně zmizí.

### Radnice

Písař, když si bere knihu ze stejné knihovny do které vracel má šanci, že na to bude mít připomínku o tom že si bere stejnou knihu.

## Diagramy

### Písař

<img width="350" alt="diagram_pisar" src="Assets/Screenshots/Diagrams/Diagram_Scribe.png">

### Králík

<img width="350" alt="diagram_zajic" src="Assets/Screenshots/Diagrams/Diagram_Rabbit.png">
