﻿using UnityEngine;
using UnityEngine.AI;

namespace Nudle.Scripts.AI
{
    public class BeerMaster : MonoBehaviour
    {
        [SerializeField]
        private AIState State = AIState.WAITING;

        public Transform Barrels;
        public Transform Beers;
        public Transform WaitingPlace;
        public NavMeshAgent Agent;
        public Animator Animator;
        public BeerManager Manager;

        public GameObject HeldItem;

        private float Timer;
        private bool ShouldGetBeers;

        private bool paused;
        private bool shouldWalk;

        public void Pause()
        {
            paused = true;
            shouldWalk = Animator.GetBool("isWalking");
            Animator.SetBool("isWalking", false);
        }

        public void Unpause()
        {
            paused = false;
            Animator.SetBool("isWalking", shouldWalk);
        }

        internal void Update()
        {
            if (paused)
                return;

            switch (State)
            {
                case AIState.WAITING:
                    if (Agent.remainingDistance <= 0.01f)
                        Animator.SetBool("isWalking", false);
                    if (ShouldGetBeers)
                    {
                        State = AIState.GETTING_BEERS;
                        Agent.destination = Barrels.position;
                        Animator.SetBool("isWalking", true);
                        ShouldGetBeers = false;
                    }
                    break;

                case AIState.GETTING_BEERS:
                    if (Agent.remainingDistance <= 0.01f)
                    {
                        State = AIState.GRABBING;
                        Timer = 2.5f;
                        Animator.SetTrigger("Grab");
                        Animator.SetBool("isWalking", false);
                    }
                    break;

                case AIState.RETURNING:
                    if (Agent.remainingDistance <= 0.01f)
                    {
                        State = AIState.PLACING_BEERS;
                        Timer = 2.5f;
                        Animator.SetTrigger("Place");
                        Animator.SetBool("isWalking", false);
                    }
                    break;

                case AIState.GRABBING:
                    Timer -= Time.deltaTime;
                    if (Timer <= 0f)
                    {
                        Agent.destination = Beers.position;
                        State = AIState.RETURNING;
                        Animator.SetBool("isWalking", true);
                        Animator.SetBool("hasBeer", true);
                        HeldItem.SetActive(true);
                    }
                    break;

                case AIState.PLACING_BEERS:
                    Timer -= Time.deltaTime;
                    if (Timer <= 0f)
                    {
                        Agent.destination = WaitingPlace.position;
                        State = AIState.WAITING;
                        Manager.SendMessage("PlaceBeers");
                        Animator.SetBool("isWalking", true);
                        Animator.SetBool("hasBeer", false);
                        HeldItem.SetActive(false);
                    }
                    break;
            }
        }

        public void GrabBeers()
        {
            if (State == AIState.WAITING)
            {
                ShouldGetBeers = true;
            }
        }

        private enum AIState
        {
            WAITING,
            GETTING_BEERS,
            RETURNING,
            GRABBING,
            PLACING_BEERS,
        }
    }
}